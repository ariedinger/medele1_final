#!/bin/bash

# Comando para leer los datos desde el puerto serie (ajústalo según tu configuración)
SERIAL_COMMAND="cat /dev/ttyUSB0"

# Configuración de gnuplot
GNUPLOT_COMMAND="gnuplot -p -e 'set xlabel \"Muestra\"; set ylabel \"Temperatura\"; set title \"Temperatura en Tiempo Real\"; plot \"-\" using 1 with lines title \"Temperatura\"'"

# Ejecuta el comando de gnuplot en un proceso en segundo plano
eval $GNUPLOT_COMMAND &

# Lee los datos del puerto serie y envíalos a gnuplot
$SERIAL_COMMAND | while read line
do
  echo $line > gnuplotfifo
done
