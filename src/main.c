#include "libs/main.h"

int main(void) {
	INIT_ALL();

	while (1) {
		if (flagMenuPpal){
			if (flagDown)
				BUTTON_DOWN();
			else if (flagEnter)
				BUTTON_ENTER();
			else if (flagUp)
				BUTTON_UP();
		}
		else if (flagTemp)
			UPDATE_TEMP();
		else if (flagOffset)
			UPDATE_OFFSET();
		else if (flagGain)
			UPDATE_GAIN();
		else if (flagModoUart)
			ADC_UART();
	}
}
