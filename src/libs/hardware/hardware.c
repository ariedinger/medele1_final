#include "libs/hardware/hardware.h"
#include "libs/leds/leds.h"
#include "libs/lcd/LCD.h"
#include "libs/menu/menu.h"
#include "libs/exti/exti.h"
#include "libs/adc/adc.h"
#include "libs/uart/uart.h"
#include "libs/timers/timers.h"
#include "libs/flash/flash.h"

void INIT_ALL(void) {
	SystemInit();

	/* ~ ~ Red LED ~ ~ */
	ledRed.port = GPIOB;
	ledRed.pin = GPIO_Pin_14;
	ledRed.clock = RCC_AHB1Periph_GPIOB;
	INIT_LEDS(ledRed);

	//GPIO_SetBits(ledRed.port, ledRed.pin);

	/* ~ ~ Blue LED ~ ~ */
	ledBlue.port = GPIOB;
	ledBlue.pin = GPIO_Pin_7;
	ledBlue.clock = RCC_AHB1Periph_GPIOB;
	INIT_LEDS(ledBlue);

	GPIO_SetBits(ledBlue.port, ledBlue.pin);

	/* ~ ~ Green LED ~ ~ */
	ledGreen.port = GPIOB;
	ledGreen.pin = GPIO_Pin_0;
	ledGreen.clock = RCC_AHB1Periph_GPIOB;
	INIT_LEDS(ledGreen);

	GPIO_SetBits(ledGreen.port, ledGreen.pin);

	/* Systick config per 1 useg */
	SysTick_Config(SystemCoreClock / 1000);

	/* ~ ~ Timer TIM3 config ~ ~ */
	INIT_TIM3(FREC_MUESTREO);
	DMA_Cmd(DMA2_Stream0, DISABLE);
	TIM_Cmd(TIM3, DISABLE);

	/* ~ ~ LCD ~ ~ */
	LCD_init();
	menuOption = 0;
	SCREENA1();

	/* ~ ~ Enter button ~ ~ */
	buttonEnter.port = GPIOD;
	buttonEnter.pin = GPIO_Pin_3;
	buttonEnter.clock = RCC_AHB1Periph_GPIOD;
	buttonEnter.exti.portSource = EXTI_PortSourceGPIOD;
	buttonEnter.exti.pinSource = EXTI_PinSource3;
	buttonEnter.exti.extiLine = EXTI_Line3;
	buttonEnter.exti.extiVector = EXTI3_IRQn;
	EXTI_INIT(buttonEnter);

	flagEnter = 0;

	/* ~ ~ Up button ~ ~ */
	buttonUp.port = GPIOD;
	buttonUp.pin = GPIO_Pin_1;
	buttonUp.clock = RCC_AHB1Periph_GPIOD;
	buttonUp.exti.portSource = EXTI_PortSourceGPIOD;
	buttonUp.exti.pinSource = EXTI_PinSource1;
	buttonUp.exti.extiLine = EXTI_Line1;
	buttonUp.exti.extiVector = EXTI1_IRQn;
	EXTI_INIT(buttonUp);

	flagUp = 0;

	/* ~ ~ Down button ~ ~ */
	buttonDown.port = GPIOD;
	buttonDown.pin = GPIO_Pin_0;
	buttonDown.clock = RCC_AHB1Periph_GPIOD;
	buttonDown.exti.portSource = EXTI_PortSourceGPIOD;
	buttonDown.exti.pinSource = EXTI_PinSource0;
	buttonDown.exti.extiLine = EXTI_Line0;
	buttonDown.exti.extiVector = EXTI0_IRQn;
	EXTI_INIT(buttonDown);

	flagDown = 0;

	/* ~ ~ Down button ~ ~ */
	buttonBack.port = GPIOA;
	buttonBack.pin = GPIO_Pin_4;
	buttonBack.clock = RCC_AHB1Periph_GPIOA;
	buttonBack.exti.portSource = EXTI_PortSourceGPIOA;
	buttonBack.exti.pinSource = EXTI_PinSource4;
	buttonBack.exti.extiLine = EXTI_Line4;
	buttonBack.exti.extiVector = EXTI4_IRQn;
	EXTI_INIT(buttonBack);

	flagBack = 0;

	/* ~ ~ Temperature sensor  ~ ~ */
	tempSen.port = GPIOC;
	tempSen.pin = GPIO_Pin_3;
	tempSen.clock = RCC_AHB1Periph_GPIOC;
	tempSen.adc.clock = RCC_APB2Periph_ADC1;
	tempSen.adc.resolution = ADC_Resolution_12b;
	tempSen.adc.nAdc = ADC1;
	tempSen.adc.injectedChannel = ADC_InjectedChannel_1;
	tempSen.adc.channel = ADC_Channel_13;
	tempSen.adc.bufferSize = SAMP_SIZE;
	ADC_INIT(tempSen, adcArray);

	flagAdc = 0;
	prevFilteredValue = 0.0f;
	flagOffset = 0;
	flagUpdateDisplay = 0;
	flagGain = 0;
	flagTemp = 0;
	temp = 0.0f;
//	offset = 2.0f;
//	FLASH_WRITE_OFFSET(offset);
//	gain = 1.0f;
//	FLASH_WRITE_GAIN(gain);
	flagFs1 = 0;
	flagFs2 = 1;
	flagMenuPpal = 1;

	fastMovingAverageIndex = 0;
	slowMovingAverageIndex = 0;

	/* ~ ~ RS232 ~ ~ */
	rs232.port = GPIOD;
	rs232.clock = RCC_AHB1Periph_GPIOD;
	rs232.uart.clock = RCC_APB1Periph_USART2;
	rs232.uart.nUart = USART2;
	rs232.uart.nUartGpio = GPIO_AF_USART2;
	rs232.uart.pinTx = GPIO_Pin_5;
	rs232.uart.pinRx = GPIO_Pin_6;
	rs232.uart.pinSourceTx = GPIO_PinSource5;
	rs232.uart.pinSourceRx = GPIO_PinSource6;
	rs232.uart.baudRate = 9600;
	rs232.uart.irq = USART2_IRQn;
	UART_INIT(rs232);

	flagUart = 0;
	flagModoUart = 0;
}
