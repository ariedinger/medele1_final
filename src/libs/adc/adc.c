#include "libs/libs.h"
#include "libs/adc/adc.h"
#include "libs/menu/menu.h"
#include "libs/sd/sd.h"
#include "libs/exti/exti.h"
#include "libs/flash/flash.h"

void ADC_INIT(struct hardware conv, uint16_t* adcArray) {
	/*Habilitacion del clock de los perficos del DMA y el ADC1::*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	/*Inicio configuracion DMA2:*/
	DMA_DeInit(DMA2_Stream0);
	/*Se elige DMA canal 0 para trabajar con ADC1:*/
	DMA_InitStructure.DMA_Channel = DMA_Channel_0;
	/*Registro donde se guardan los valores convertidos:*/
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &ADC1->DR;
	/*Direccion del arreglo donde se guardan los valores leidos:*/
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) adcArray;
	/*Establecer que el DMA transmitira a memoria:*/
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	/*Establecer la cantidad de valores a convertir:*/
	DMA_InitStructure.DMA_BufferSize = conv.adc.bufferSize;
	/*Se deshabilita el incremento de memoria por periferico:*/
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	/*Se habilita el incremento de memoria para crear un arreglo dinamico:*/
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	/*Ancho de bit del dato:*/
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	/*Ancho de cada elemento del arreglo:*/
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	/*Modo circuilar, cuando llega al final del arreglo vuelve a arrancar desde el primer valor:*/
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	/*Maxima prioridad al DMA:*/
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA2_Stream0, &DMA_InitStructure);

	/*Configuracion del handler para la interrupcion:*/
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	DMA_ITConfig(DMA2_Stream0, DMA_IT_TC, ENABLE);

	/*Habilitacion del DMA:*/
	DMA_Cmd(DMA2_Stream0, ENABLE);

	/*Inicio configuracion del ADC1:*/
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
	/*Se habilita el acceso del DMA:*/
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_1;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;

	/*Cargar la configuracion:*/
	ADC_CommonInit(&ADC_CommonInitStructure);

	/*Resolucion del ADC en 12 bits:*/
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	/*Habilita la conversion de varios canales simultaneamente:*/
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	/*Se deshabilita el modo continuo, ya que el ADC se va a activar por el TIM3:*/
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	/*Disparo por flanco ascendente:*/
	ADC_InitStructure.ADC_ExternalTrigConvEdge =
	ADC_ExternalTrigConvEdge_Rising;
	/*Define el TMRGO del TIM3 como disparo del ADC:*/
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T3_TRGO;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	/*Cantidad de datos que se van a convertir por disparo del ADC:*/
	ADC_InitStructure.ADC_NbrOfConversion = 1;

	/*Inicializacion del ADC con los parametros establecidos:*/
	ADC_Init(conv.adc.nAdc, &ADC_InitStructure);

	/*Configuracion de los canales del ADC:*/
	/*Canal 10 ADC1 (PC0) con orden 1 para conversion de tension:*/
	ADC_RegularChannelConfig(conv.adc.nAdc, conv.adc.channel, 1,
	ADC_SampleTime_480Cycles);
	/*Habilitacion del pedido del DMA en el ADC1:*/
	ADC_DMARequestAfterLastTransferCmd(conv.adc.nAdc, ENABLE);
	ADC_DMACmd(conv.adc.nAdc, ENABLE);

	RCC_AHB1PeriphClockCmd(conv.adc.clock, ENABLE);

	/* PC1 para entrada analógica */
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = conv.pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(conv.port, &GPIO_InitStructure);

	/*Habilitacion del ADC1:*/
	ADC_Cmd(conv.adc.nAdc, ENABLE);

	DMA_Cmd(DMA2_Stream0, DISABLE);
	TIM_Cmd(TIM3, DISABLE);
}

/*Interrupcion por Transfer Request del DMA:*/
void DMA2_Stream0_IRQHandler(void) {
	/* transmission complete interrupt */
	if (DMA_GetFlagStatus(DMA2_Stream0, DMA_FLAG_TCIF0)) {
		/*Se habilita el flag para procesar los datos:*/
		flagAdc = 1;

		/*Resetear el flag del DMA:*/
		DMA_ClearFlag(DMA2_Stream0, DMA_FLAG_TCIF0);
	}
}

float FAST_MOVING_AVERAGE(float inputValue) {
	fastMovingAverageBuffer[fastMovingAverageIndex] = inputValue;
	fastMovingAverageIndex = (fastMovingAverageIndex + 1)
			% FAST_MOVING_AVERAGE_SIZE;

	float sum = 0;
	for (int i = 0; i < FAST_MOVING_AVERAGE_SIZE; ++i) {
		sum += fastMovingAverageBuffer[i];
	}
	return sum / FAST_MOVING_AVERAGE_SIZE;
}

float SLOW_MOVING_AVERAGE(float inputValue) {
	slowMovingAverageBuffer[slowMovingAverageIndex] = inputValue;
	slowMovingAverageIndex = (slowMovingAverageIndex + 1)
			% SLOW_MOVING_AVERAGE_SIZE;

	float sum = 0;
	for (int i = 0; i < SLOW_MOVING_AVERAGE_SIZE; ++i) {
		sum += slowMovingAverageBuffer[i];
	}
	return sum / SLOW_MOVING_AVERAGE_SIZE;
}

float DIG_TO_VOLT(int dig) {
	/* 4095 dig count   --------> 3 V
	 * Actual dig count --------> x V */
	return (float) dig * 3.3 / 4095;
}

float VOLT_TO_TEMP(float volt) {
	return volt * 10;
}

float CALCULATE_TEMP(int dig) {
	if (flagFs2)
		return SLOW_MOVING_AVERAGE(VOLT_TO_TEMP((DIG_TO_VOLT(dig))));
	else if (flagFs1)
		return FAST_MOVING_AVERAGE(VOLT_TO_TEMP((DIG_TO_VOLT(dig))));
	else
		return -1;
}

void CALCULATE_OFFSET(void) {
	if (flagDown) {
		flagDown = 0;
		offset = FLASH_READ(OFFSET_ADDR) - 0.1;
		FLASH_WRITE_OFFSET(offset);
	} else if (flagUp) {
		flagUp = 0;
		offset = FLASH_READ(OFFSET_ADDR) + 0.1;
		FLASH_WRITE_OFFSET(offset);
	}
}

void CALCULATE_GAIN(void) {
	if (flagDown) {
		flagDown = 0;
		gain = FLASH_READ(GAIN_ADDR) - 0.1;
		FLASH_WRITE_GAIN(gain);
	} else if (flagUp) {
		flagUp = 0;
		gain = FLASH_READ(GAIN_ADDR) + 0.1;
		FLASH_WRITE_GAIN(gain);
	}
}

void PRINT_TEMP(void) {
	sprintf(bufferAdc, "   T : %.2f C",
			temp * FLASH_READ(GAIN_ADDR) + FLASH_READ(OFFSET_ADDR));
	LCD_clrscr();
	LCD_WriteString(0, 0, bufferAdc);
}

void UPDATE_TEMP(void) {
	if (flagBack) {
		GO_BACK();
	}

	if (flagAdc) {
		flagAdc = 0;
		flagUpdateDisplay++;
		temp = CALCULATE_TEMP(adcArray[0]);
	}

	UPDATE_DISPLAY();
}

void UPDATE_OFFSET(void) {
	CALCULATE_OFFSET();

	if (flagBack) {
		GO_BACK();
	}

	if (flagAdc) {
		flagAdc = 0;
		flagUpdateDisplay++;
		temp = CALCULATE_TEMP(adcArray[0]);
	}

	UPDATE_DISPLAY();
}

void UPDATE_GAIN(void) {
	CALCULATE_GAIN();

	if (flagBack) {
		GO_BACK();
	}

	if (flagAdc) {
		flagAdc = 0;
		flagUpdateDisplay++;
		temp = CALCULATE_TEMP(adcArray[0]);
	}

	UPDATE_DISPLAY();
}

void UPDATE_DISPLAY(void) {
	if (flagUpdateDisplay == FREC_MUESTREO/2) {
		flagUpdateDisplay = 0;
		PRINT_TEMP();
		if (flagOffset){
			sprintf(bufferCalibre, "   O : %.1f C", FLASH_READ(OFFSET_ADDR));
			LCD_WriteString(0, 1, bufferCalibre);
		}
		else if (flagGain){
			sprintf(bufferCalibre, "   S : %.1f C", FLASH_READ(GAIN_ADDR));
			LCD_WriteString(0, 1, bufferCalibre);
		}
	}
}

void ADC_UART(void) {
	if (flagBack) {
		GO_BACK();
	}

	if (flagAdc) {
		flagAdc = 0;

		sprintf(bufferAdc, "%.2f,", CALCULATE_TEMP(adcArray[0]) * FLASH_READ(GAIN_ADDR) + FLASH_READ(OFFSET_ADDR));
		UART_SEND_STRING(rs232, bufferAdc);
	}
}
