#ifndef ADC_H
#define ADC_H

#include "libs/hardware/hardware.h"

#define SAMP_SIZE 1

#define FAST_MOVING_AVERAGE_SIZE 50
#define SLOW_MOVING_AVERAGE_SIZE 500

GPIO_InitTypeDef GPIO_InitStructure;
ADC_InitTypeDef ADC_InitStructure;
ADC_CommonInitTypeDef ADC_CommonInitStructure;
DMA_InitTypeDef DMA_InitStructure;

void ADC_INIT(struct hardware conv, uint16_t* adcArray);
int ADC_READ(struct hardware conv);
float DIG_TO_VOLT(int dig);
float VOLT_TO_TEMP(float volt);
float ADC_SAMPLE(struct hardware sen);
void ADC_PROCESSING(void);
void ADC_UART(void);
void UPDATE_TEMP(void);
void UPDATE_OFFSET(void);
void UPDATE_GAIN(void);
void CALCULATE_OFFSET(void);
void CALCULATE_GAIN(void);
float CALCULATE_TEMP(int dig);
float IIR_FILTER(float inputValue);
void PRINT_TEMP(void);
void UPDATE_DISPLAY(void);

char bufferAdc[20];
char bufferCalibre[20];
int flagUpdateDisplay;
int flagAdc;
int flagOffset;
int flagGain;
int flagTemp;
int flagFs1, flagFs2;
uint16_t adcArray[SAMP_SIZE];
float prevFilteredValue;
float offset;
float gain;
float flagMenuPpal;
float temp;

float fastMovingAverageBuffer[FAST_MOVING_AVERAGE_SIZE];
int fastMovingAverageIndex;
float slowMovingAverageBuffer[SLOW_MOVING_AVERAGE_SIZE];
int slowMovingAverageIndex;

#endif
