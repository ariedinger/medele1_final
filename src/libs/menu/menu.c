#include "libs/menu/menu.h"
#include "libs/adc/adc.h"
#include "libs/timers/timers.h"
#include "libs/exti/exti.h"

void MENU_DOWN(int menuOption) {
	switch (menuOption) {
	case SCREEN_A2:
		SCREENA2();
		break;
	case SCREEN_B1:
		SCREENB1();
		break;
	case SCREEN_B2:
		SCREENB2();
		break;
	case SCREEN_C1:
		SCREENC1();
		break;
	case SCREEN_C2:
		SCREENC2();
		break;
	case SCREEN_D1:
		SCREEND1();
		break;
	}
}

void MENU_UP(int menuOption) {
	switch (menuOption) {
	case SCREEN_A1:
		SCREENA1();
		break;
	case SCREEN_A2:
		SCREENA2();
		break;
	case SCREEN_B1:
		SCREENB1();
		break;
	case SCREEN_B2:
		SCREENB2();
		break;
	case SCREEN_C1:
		SCREENC1();
		break;
	case SCREEN_C2:
		SCREENC2();
		break;
	}
}

void MENU_ENTER(int menuOption) {
	switch (menuOption) {
	case SCREEN_A1:
		FUNC_A1();
		break;
	case SCREEN_A2:
		FUNC_A2();
		break;
	case SCREEN_B1:
		FUNC_B1();
		break;
	case SCREEN_B2:
		FUNC_B2();
		break;
	case SCREEN_C1:
		FUNC_C1();
		break;
	case SCREEN_C2:
		FUNC_C2();
		break;
	case SCREEN_D1:
		FUNC_D1();
		break;
	}
}

void SCREENA1(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "-> Mostrar Temp.");
	LCD_WriteString(0, 1, "   Modo UART.");
}

void SCREENA2(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "   Mostrar Temp.");
	LCD_WriteString(0, 1, "-> Modo UART.");
}

void SCREENB1(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "-> Rta. Lenta.");
	LCD_WriteString(0, 1, "   Rta. Rapida.");
}

void SCREENB2(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "   Rta. Lenta.");
	LCD_WriteString(0, 1, "-> Rta. Rapida.");
}

void SCREENC1(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "-> Cal. Offset.");
	LCD_WriteString(0, 1, "   Cal. Span.");
}

void SCREENC2(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "   Cal. Offset.");
	LCD_WriteString(0, 1, "-> Cal. Span.");
}

void SCREEND1(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "-> Info.");
}

void FUNC_A1() {
	if (!flagTemp)
	{
		LCD_clrscr();
		LCD_WriteString(0, 0, "Tomando muestras");
		LCD_WriteString(0, 1, "Por favor espere");

		flagMenuPpal = 0;
		flagTemp = 1;
		flagOffset = 0;
		flagGain = 0;

		DMA_Cmd(DMA2_Stream0, ENABLE);
		TIM_Cmd(TIM3, ENABLE);
	}

	UPDATE_TEMP();
}

void FUNC_A2() {
	if (!flagModoUart)
	{
		LCD_clrscr();
		LCD_WriteString(0, 0, "Entrando a ");
		LCD_WriteString(0, 1, "Modo UART");

		flagModoUart = 1;
		flagMenuPpal = 0;
		flagTemp = 0;
		flagOffset = 0;
		flagGain = 0;

		DMA_Cmd(DMA2_Stream0, ENABLE);
		TIM_Cmd(TIM3, ENABLE);
	}
	ADC_UART();
}

void FUNC_B1() {
	LCD_clrscr();
	LCD_WriteString(0, 0, "Respuesta Lenta:");
	LCD_WriteString(0, 1, "N = 500");

	flagFs1 = 0;
	flagFs2 = 1;

	if(flagBack)
	{
		GO_BACK();
		LCD_clrscr();
		SCREENB1();
	}
}

void FUNC_B2(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "Respuesta Rapida:");
	LCD_WriteString(0, 1, "N = 50");

	flagFs1 = 1;
	flagFs2 = 0;

	if(flagBack)
	{
		GO_BACK();
		LCD_clrscr();
		SCREENB2();
	}
}

void FUNC_C1(void) {
	if (!flagOffset)
	{
		LCD_clrscr();
		LCD_WriteString(0, 0, "Tomando muestras");
		LCD_WriteString(0, 1, "Por favor espere");

		flagMenuPpal = 0;
		flagOffset = 1;
		flagGain = 0;
		flagTemp = 0;

		DMA_Cmd(DMA2_Stream0, ENABLE);
		TIM_Cmd(TIM3, ENABLE);
	}
	UPDATE_OFFSET();
}

void FUNC_C2(void) {
	if (!flagGain)
	{
		LCD_clrscr();
		LCD_WriteString(0, 0, "Tomando muestras");
		LCD_WriteString(0, 1, "Por favor espere");
		flagMenuPpal = 0;
		flagOffset = 0;
		flagGain = 1;
		flagTemp = 0;

		DMA_Cmd(DMA2_Stream0, ENABLE);
		TIM_Cmd(TIM3, ENABLE);
	}
	UPDATE_GAIN();
}

void FUNC_D1(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "version 1.1");
	if(flagBack)
	{
		GO_BACK();
		LCD_clrscr();
		SCREEND1();
	}
}

void BUTTON_DOWN(void) {
	flagDown = 0;
	menuOption++;
	flagTemp = 0;
	if (menuOption > SCREEN_D1)
		menuOption = SCREEN_D1;
	MENU_DOWN(menuOption);
}

void BUTTON_UP(void) {
	flagUp = 0;
	menuOption--;
	if (menuOption < SCREEN_A1)
		menuOption = SCREEN_A1;
	MENU_UP(menuOption);
}

void BUTTON_ENTER(void) {
	flagEnter = 0;
	MENU_ENTER(menuOption);
}
