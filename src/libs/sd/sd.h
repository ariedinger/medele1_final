#ifndef LIBS_SD_SD_H_
#define LIBS_SD_SD_H_

#include "libs/libs.h"
#include "libs/hardware/hardware.h"
#include "libs/sd/tm_stm32f4_fatfs/tm_stm32f4_fatfs.h"
char buffSD[10];
float tempSD[10];
FATFS FatFs;
FIL fil;

void WRITE_SD(char* buffer, int isCreatedOrOpened);

#endif /* LIBS_SD_SD_H_ */
