#ifndef LIBS_FLASH_FLASH_H_
#define LIBS_FLASH_FLASH_H_

#include "libs/libs.h"

#define OFFSET_ADDR   ((uint32_t)0x080E0000)
#define GAIN_ADDR   ((uint32_t)0x08040000)

void FLASH_WRITE_OFFSET(float value);
void FLASH_WRITE_GAIN(float value);
float FLASH_READ(uint32_t address);

#endif /* LIBS_FLASH_FLASH_H_ */
