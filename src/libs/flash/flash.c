#include "libs/flash/flash.h"

#include <stdint.h>

void FLASH_WRITE_OFFSET(float value) {
    // Desbloquea la programación de la Flash
    FLASH_Unlock();

    // Asegúrate de que la Flash está lista para la programación
    while (FLASH_GetFlagStatus(FLASH_FLAG_BSY) != RESET);

    // Borra la página de la Flash antes de escribir
    FLASH_EraseSector(FLASH_Sector_11, VoltageRange_3);

    // Programa el valor en la Flash
    uint32_t data = 0;
    memcpy(&data, &value, sizeof(float));
    FLASH_ProgramWord(OFFSET_ADDR, data);

    // Bloquea la programación de la Flash
    FLASH_Lock();
}

void FLASH_WRITE_GAIN(float value) {
    // Desbloquea la programación de la Flash
    FLASH_Unlock();

    // Asegúrate de que la Flash está lista para la programación
    while (FLASH_GetFlagStatus(FLASH_FLAG_BSY) != RESET);

    // Borra la página de la Flash antes de escribir
    FLASH_EraseSector(FLASH_Sector_6, VoltageRange_3);

    // Programa el valor en la Flash
    uint32_t data = 0;
    memcpy(&data, &value, sizeof(float));
    FLASH_ProgramWord(GAIN_ADDR, data);

    // Bloquea la programación de la Flash
    FLASH_Lock();
}

float FLASH_READ(uint32_t address) {
    uint32_t data = *(uint32_t*)address;
    float value = 0.0f;
    memcpy(&value, &data, sizeof(float));
    return value;
}
