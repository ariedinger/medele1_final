#include "libs/timers/timers.h"

void INIT_TIM3(uint32_t Freq)
{
	/*Habilitacion del clock del TIM3:*/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	/*Computacion del valor del preescaler:*/
	uint32_t TimeBase = 200e3;
	uint16_t PrescalerValue = (uint16_t) ((SystemCoreClock / 2) / TimeBase) - 1;


	/*Configuracion del tiempo de interrupcion:*/
	TIM_TimeBaseStructure.TIM_Period = TimeBase / Freq - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* Selección de TIM3 TRGO */
	TIM_SelectOutputTrigger(TIM3, TIM_TRGOSource_Update);
	TIM_Cmd(TIM3, DISABLE);
}
