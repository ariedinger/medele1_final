#ifndef LIBS_TIMERS_TIMERS_H_
#define LIBS_TIMERS_TIMERS_H_

#include "libs/libs.h"

#define FREC_MUESTREO 100

TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
TIM_OCInitTypeDef TIM_OCInitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

void INIT_TIM3(uint32_t freq);

uint8_t flagTim3;

#endif /* LIBS_TIMERS_TIMERS_H_ */
