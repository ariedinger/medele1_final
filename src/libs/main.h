#ifndef LIBS_MAIN_H_
#define LIBS_MAIN_H_

#include "libs/libs.h"
#include "libs/lcd/LCD.h"
#include "libs/leds/leds.h"
#include "libs/hardware/hardware.h"
#include "libs/exti/exti.h"
#include "libs/menu/menu.h"
#include "libs/adc/adc.h"
#include "libs/uart/uart.h"
#include "libs/timers/timers.h"

uint8_t flagSystick = 0;

#endif /* LIBS_MAIN_H_ */
