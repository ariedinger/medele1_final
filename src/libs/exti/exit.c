#include "libs/exti/exti.h"
#include "libs/adc/adc.h"
#include "libs/menu/menu.h"
#include "libs/lcd/LCD.h"

void EXTI_INIT(struct hardware button)
{
	/* Enable clock for GPIOD */
	RCC_AHB1PeriphClockCmd(button.clock, ENABLE);
	/* Enable clock for SYSCFG */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Set pin as input */
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = button.pin;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(button.port, &GPIO_InitStruct);

	/* Tell system that you will use PD0 for EXTI_Line0 */
	SYSCFG_EXTILineConfig(button.exti.portSource, button.exti.pinSource);

	/* PD0 is connected to EXTI_Line0 */
	EXTI_InitStruct.EXTI_Line = button.exti.extiLine;
	/* Enable interrupt */
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	/* Interrupt mode */
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	/* Triggers on rising and falling edge */
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	/* Add to EXTI */
	EXTI_Init(&EXTI_InitStruct);

	/* Add IRQ vector to NVIC */
	/* PD0 is connected to EXTI_Line0, which has EXTI0_IRQn vector */
	NVIC_InitStruct.NVIC_IRQChannel = button.exti.extiVector;
	/* Set priority */
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	/* Set sub priority */
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	/* Enable interrupt */
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	/* Add to NVIC */
	NVIC_Init(&NVIC_InitStruct);
}

void EXTI0_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
		flagEnter = 1;
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
}

void EXTI3_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line3) != RESET) {
		flagDown = 1;
		EXTI_ClearITPendingBit(EXTI_Line3);
	}
}

void EXTI1_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line1) != RESET) {
		flagUp = 1;
		EXTI_ClearITPendingBit(EXTI_Line1);
	}
}

void EXTI4_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line4) != RESET) {
		flagBack = 1;
		EXTI_ClearITPendingBit(EXTI_Line4);
	}
}

void GO_BACK(){
	DMA_Cmd(DMA2_Stream0, DISABLE);
	TIM_Cmd(TIM3, DISABLE);

	flagMenuPpal = 1;
	flagBack = 0;
	flagAdc = 0;
	LCD_clrscr();

	if(flagTemp){
		flagTemp = 0;
		SCREENA1();
	}
	else if(flagOffset){
		flagOffset = 0;
		SCREENC1();
	}
	else if(flagGain){
		flagGain = 0;
		SCREENC2();
	}
}

