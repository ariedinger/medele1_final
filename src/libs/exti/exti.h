#ifndef LIBS_LCD_LCD_H_
#define LIBS_LCD_LCD_H_

#include "libs/libs.h"
#include "libs/hardware/hardware.h"

GPIO_InitTypeDef GPIO_InitStruct;
EXTI_InitTypeDef EXTI_InitStruct;
NVIC_InitTypeDef NVIC_InitStruct;

void EXTI_INIT(struct hardware button);
void GO_BACK(void);

uint8_t flagEnter, flagDown, flagUp, flagBack;
int menuOption;

#endif /* LIBS_LCD_LCD_H_ */
