#include "libs/uart/uart.h"

void UART_INIT(struct hardware dev) {
	/* --------------------------- System Clocks Configuration -----------------*/
	/* USART2 clock enable */
	RCC_APB1PeriphClockCmd(dev.uart.clock, ENABLE);

	/* GPIOA clock enable */
	RCC_AHB1PeriphClockCmd(dev.clock, ENABLE);

	/*-------------------------- GPIO Configuration ----------------------------*/
	GPIO_InitStructure.GPIO_Pin = dev.uart.pinTx | dev.uart.pinRx;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(dev.port, &GPIO_InitStructure);

	/* Connect USART pins to AF */
	GPIO_PinAFConfig(dev.port, dev.uart.pinSourceTx, dev.uart.nUartGpio);
	GPIO_PinAFConfig(dev.port, dev.uart.pinSourceRx, dev.uart.nUartGpio);

	/* USARTx configuration ------------------------------------------------------*/
	USART_InitStructure.USART_BaudRate = dev.uart.baudRate;
	USART_InitStructure.USART_WordLength = USART_WordLength_9b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_Even;
	USART_InitStructure.USART_HardwareFlowControl =
	USART_HardwareFlowControl_None;

	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(dev.uart.nUart, &USART_InitStructure);

	USART_Cmd(dev.uart.nUart, ENABLE);

	/* Enable RX interrupt */
	USART_ITConfig(dev.uart.nUart, USART_IT_RXNE, ENABLE);

	NVIC_InitStruct.NVIC_IRQChannel = dev.uart.irq;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 2;
	NVIC_Init(&NVIC_InitStruct);

}

void UART_SEND_STRING(struct hardware dev, char* message) {
	for (int i = 0; i <= strlen(message); i++) {
		while (USART_GetFlagStatus(dev.uart.nUart, USART_FLAG_TXE) == RESET) {
			// Do nothing, wait for the USART_FLAG_TXE to be set
		}
		USART_SendData(dev.uart.nUart, message[i]);
	}
}

void UART_CHANGE_SPEED(struct hardware dev, int speed)
{
	USART_Cmd(dev.uart.nUart, DISABLE);
	dev.uart.baudRate = speed;
	UART_INIT(dev);
}

void USART2_IRQHandler(void) {
	if (USART_GetFlagStatus(USART2, USART_IT_RXNE) == SET) {
			flagUart = 1;
	}
	USART_ClearITPendingBit(USART2, USART_IT_RXNE);
}
