% Parámetros del filtro de promedio móvil
N1 = 500;  % Tamaño del buffer para el primer caso
N2 = 50;   % Tamaño del buffer para el segundo caso

% Definir la función de transferencia para el primer caso
numerator1 = ones(1, N1);
denominator1 = [1, zeros(1, N1 - 1)];

% Definir la función de transferencia para el segundo caso
numerator2 = ones(1, N2);
denominator2 = [1, zeros(1, N2 - 1)];

% Respuesta en frecuencia
freq = 0:0.01:pi;  % Frecuencias de 0 a pi

% Calcular la respuesta en frecuencia para ambos casos
H1 = freqz(numerator1, denominator1, freq);
H2 = freqz(numerator2, denominator2, freq);

% Graficar la magnitud de la respuesta en frecuencia para ambos casos
figure;
plot(freq/pi, abs(H1), 'b', 'LineWidth', 2, 'DisplayName', ['N = ', num2str(N1)]);
hold on;
plot(freq/pi, abs(H2), 'r', 'LineWidth', 2, 'DisplayName', ['N = ', num2str(N2)]);
hold off;

title('Respuesta en Frecuencia del Filtro de Promedio Móvil');
xlabel('Frecuencia Normalizada (\omega / \pi)');
ylabel('Magnitud');
grid on;

% Agregar leyenda
legend('Location', 'Best');
